#include "AppClass.h"
void Application::InitVariables(void)
{
	
	//Set the position and target of the camera
	//(I'm at [0,0,10], looking at [0,0,0] and up is the positive Y axis)
	m_pCameraMngr->SetPositionTargetAndUp(AXIS_Z * 20.0f, ZERO_V3, AXIS_Y);

	//if the light position is zero move it
	if (m_pLightMngr->GetPosition(1) == ZERO_V3)
		m_pLightMngr->SetPosition(vector3(0.0f, 0.0f, 3.0f));

	//if the background is cornflowerblue change it to black (its easier to see)
	if (vector3(m_v4ClearColor) == C_BLUE_CORNFLOWER)
	{
		m_v4ClearColor = vector4(ZERO_V3, 1.0f);
	}
	
	//if there are no segments create 7
	if(m_uOrbits < 1)
		m_uOrbits = 7;

	float fSize = 1.0f; //initial size of orbits

	//creating a color using the spectrum 
	uint uColor = 650; //650 is Red
	//prevent division by 0
	float decrements = 250.0f / (m_uOrbits > 1? static_cast<float>(m_uOrbits - 1) : 1.0f); //decrement until you get to 400 (which is violet)
	/*
		This part will create the orbits, it start at 3 because that is the minimum subdivisions a torus can have
	*/
	uint uSides = 3; //start with the minimal 3 sides

	m_sphereStopIndexTrackers = new uint[m_uOrbits];

	for (uint i = uSides; i < m_uOrbits + uSides; i++)
	{
		vector3 v3Color = WaveLengthToRGB(uColor); //calculate color based on wavelength
		m_shapeList.push_back(m_pMeshMngr->GenerateTorus(fSize, fSize - 0.1f, 3, i, v3Color)); //generate a custom torus and add it to the meshmanager

		/* initialize stop list at i */

		std::vector<vector3> stopsList;
		float subSegmentAngle = TAU / i;
		for (uint j = 0; j < i; j++)
		{
			float stopAngle = subSegmentAngle * j;
			float x = cos(stopAngle) * fSize;
			float y = sin(stopAngle) * fSize;
			stopsList.push_back(vector3(x, y, 0));
		}
		m_stopMatrix.push_back(stopsList);
		
		m_sphereStopIndexTrackers[i - uSides] = 0;
		/* end initialize stop list */

		fSize += 0.5f; //increment the size for the next orbit
		uColor -= static_cast<uint>(decrements); //decrease the wavelength
	}
}
void Application::Update(void)
{
	//Update the system so it knows how much time has passed since the last call
	m_pSystem->Update();

	//Is the arcball active?
	ArcBall();

	//Is the first person camera active?
	CameraRotation();
}
void Application::Display(void)
{
	// Clear the screen
	ClearScreen();

	matrix4 m4View = m_pCameraMngr->GetViewMatrix(); //view Matrix
	matrix4 m4Projection = m_pCameraMngr->GetProjectionMatrix(); //Projection Matrix
	matrix4 m4Offset = IDENTITY_M4; //offset of the orbits, starts as the global coordinate system
	/*
		The following offset will orient the orbits as in the demo, start without it to make your life easier.
	*/

	//Get a timer
	static float fTimer = 0;	//store the new timer
	static uint uClock = m_pSystem->GenClock(); //generate a new clock for that timer
	fTimer += m_pSystem->GetDeltaTime(uClock); //get the delta time for that timer

	float fTimeBetweenStops = m_fSpeed;//in seconds
	//map the value to be between 0.0 and 1.0
	float fPercentage = MapValue(fTimer, 0.0f, fTimeBetweenStops, 0.0f, 1.0f);

	static float offset = PI/2;

	if (m_bScaleInterp) {
		offset += 0.01;
	}

	if (offset > TAU) {
		offset = 0;
	}

	float scaleOffset = sin(offset);

	m4Offset = glm::rotate(IDENTITY_M4, m_zAngle, AXIS_Z);

	// draw a shapes
	for (uint i = 0; i < m_uOrbits; ++i)
	{
		// Draw the tuper mesh:
		m_pMeshMngr->AddMeshToRenderList(m_shapeList[i], glm::rotate(m4Offset, 1.5708f, AXIS_X) * glm::scale(vector3(scaleOffset)));

		// get a ptr to our stop list
		std::vector<vector3> * stopsListPtr = &m_stopMatrix.at(i);

		// get a ptr to the current stop index. 
		// (Later we use this pointer to mutate the index if needed)
		uint currentStopIndex = m_sphereStopIndexTrackers[i];

		uint stopsListLength = stopsListPtr->size();

		uint nextStopIndex = (currentStopIndex + 1) % stopsListLength;

		vector3 * currentStopPtr = &stopsListPtr->at(currentStopIndex);

		vector3 * nextStopPtr = &stopsListPtr->at(nextStopIndex);
		
		// calculate the current position
		vector3 v3CurrentPos = glm::lerp(*currentStopPtr, *nextStopPtr, fPercentage);

		if (fPercentage >= 0.999f) {
			m_sphereStopIndexTrackers[i] = nextStopIndex;
			fTimer = 0;
		}

		matrix4 m4SphereModel = glm::translate(m4Offset, v3CurrentPos * scaleOffset);
		
			//draw spheres
		m_pMeshMngr->AddSphereToRenderList(m4SphereModel * glm::scale(vector3(0.18)), C_WHITE);

		// delete the ptr
		stopsListPtr = nullptr;
		currentStopPtr = nullptr;
		nextStopPtr = nullptr;
	}

	//render list call
	m_uRenderCallCount = m_pMeshMngr->Render();

	//clear the render list
	m_pMeshMngr->ClearRenderList();
	
	//draw gui
	DrawGUI();
	
	//end the current frame (internally swaps the front and back buffers)
	m_pWindow->display();
}
void Application::Release(void)
{
	delete[] m_sphereStopIndexTrackers;
	//release GUI
	ShutdownGUI();
}