#include "AppClass.h"
void Application::InitVariables(void)
{
	m_pMeshMatrix = new MyMesh**[INVADER_W];

	for (int x = 0; x < INVADER_W; x++)
	{
		m_pMeshMatrix[x] = new MyMesh*[INVADER_H];
		
		for (int y = 0; y < INVADER_H; y++)
		{
			switch (INVADER_SHAPE[x][y])
			{
			case 1:
				m_pMeshMatrix[x][y] = new MyMesh();
				m_pMeshMatrix[x][y]->GenerateCube(1.0f, C_BLACK);
				m_pMeshMatrix[x][y]->SetPosition(vector3(y, -x, 0));
				break;
			case 0:
			default:
				m_pMeshMatrix[x][y] = nullptr;
				break;
			}
		}

	}
}
void Application::Update(void)
{
	//Update the system so it knows how much time has passed since the last call
	m_pSystem->Update();

	//Is the arcball active?
	ArcBall();

	//Is the first person camera active?
	CameraRotation();
}
void Application::Display(void)
{
	// Clear the screen
	ClearScreen();

	matrix4 m4View = m_pCameraMngr->GetViewMatrix();
	matrix4 m4Projection = m_pCameraMngr->GetProjectionMatrix();
	
	matrix4 m4Scale = glm::scale(IDENTITY_M4, vector3(2.0f,2.0f,2.0f));
	
	static float offset = 0.0f;
	
	offset += 0.1f;

	float xOffset = sin(offset) * 10;

	for (int x = 0; x < INVADER_W; x++)
	{
		for (int y = 0; y < INVADER_H; y++)
		{
			if (m_pMeshMatrix[x][y] != nullptr) {

				matrix4 m4Translate = glm::translate(
					IDENTITY_M4,
					m_pMeshMatrix[x][y]-> GetPosition() + 
					vector3(xOffset, 0, 0)
				);

				matrix4 m4Model = m4Scale * m4Translate;

				m_pMeshMatrix[x][y]->Render(m4Projection, m4View, m4Model);
			}
		}
	}

	// draw a skybox
	m_pMeshMngr->AddSkyboxToRenderList();
	
	//render list call
	m_uRenderCallCount = m_pMeshMngr->Render();

	//clear the render list
	m_pMeshMngr->ClearRenderList();
	
	//draw gui
	DrawGUI();
	
	//end the current frame (internally swaps the front and back buffers)
	m_pWindow->display();
}
void Application::Release(void)
{
	for (int x = 0; x < INVADER_W; x++)
	{
		for (int y = 0; y < INVADER_H; y++)
		{
			if (m_pMeshMatrix[x][y] != nullptr) {
				SafeDelete(m_pMeshMatrix[x][y]);
			}
		}
		delete[] m_pMeshMatrix[x];
	}

	delete[] m_pMeshMatrix;

	//release GUI
	ShutdownGUI();
}