/*
	Item class used for testing with the Queue
	AUTHOR: LAB
	LICENSE: MIT
*/


#include "item.h"

Item::Item()
{
	this->id = 0;
}


Item::Item(int id)
{
	this->id = id;
}

Item::~Item()
{
}

int Item::GetId() const
{
	return id;
}

std::ostream & operator<<(std::ostream & os, const Item & p)
{
	return os << "ITEM#" << p.id;
}
