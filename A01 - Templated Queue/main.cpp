/*
	IGME309.01
	A01 - Templated Queue
	Author: LAB
*/

#include "Main.h"

/*
	Create Sample Int Queue with Priority Test Case
*/
void initSampleIntQueue(Queue<int>& testQueue, int n = 10) {
	int sum = 0;

	for (int i = 0; i < n; i++) {
		sum += i;
		testQueue.Push(sum);
		testQueue.Push(i);
	}
}

/*
	Create Sample Item Queue with Priority Test Case
*/
void initSampleItemQueue(Queue<Item>& testQueue, int n = 10) {
	int sum = 0;

	for (int i = 0; i < n; i++) {
		sum += i;
		Item itemA(sum);
		testQueue.Push(itemA);

		Item itemB(i);
		testQueue.Push(itemB);
	}
}

/*
	Check if two queue has different data
*/
template <typename T>
bool checkQueueElementDiff(Queue<T>& queueA, Queue<T>& queueB) {
	LinkedListNode<T> * queueAPtr = queueA.GetHeadPtr();
	LinkedListNode<T> * queueBPtr = queueB.GetHeadPtr();

	while (queueAPtr != nullptr) {
		if (queueAPtr->GetData() != queueBPtr->GetData()) {
			return true;
		}
		queueAPtr = queueAPtr->GetNextPtr();
		queueBPtr = queueBPtr->GetNextPtr();
	}

	return false;
}

/* 
	Check if two queues differ in size, or same size.
*/
template <typename T>
bool checkQueueSize(Queue<T>& queueA, Queue<T>& queueB) {
	cout << TAG << "QUEUE A SIZE: " << queueA.GetSize() << endl;
	cout << TAG << "QUEUE B SIZE: " << queueB.GetSize() << endl;

	return queueA.GetSize() == queueB.GetSize();
}

/*
	Compare if two queue are independent and have same elements
*/
template <typename T>
bool compareTwoQueue(Queue<T>& queueA, Queue<T>& queueB) {

	cout << TAG << "QUEUE A:" << endl;
	queueA.Print();

	cout << TAG << "QUEUE B:" << endl;
	queueB.Print();

	cout << TAG << "QUEUE A ADDR: " << &queueA << endl;
	cout << TAG << "QUEUE B ADDR: " << &queueB << endl;

	if (&queueA == &queueB) {
		cout << "QUEUE A HAS THE SAME ADDRESS AS QUEUE B!" << endl;
		return false;
	}

	if (!checkQueueSize<T>(queueA, queueB)) {
		cout << TAG << "QUEUE A AND QUEUE B DIFFERS IN SIZE!" << endl;
		return false;
	}

	if (checkQueueElementDiff(queueA, queueB)) {
		cout << TAG << "QUEUE A AND QUEUE B DIFFERS IN ELEMENT!" << endl;
		return false;
	}

	queueB.Push(99);

	if (checkQueueSize(queueA, queueB)) {
		cout << TAG << "QUEUE A AND QUEUE B HAS SAME SIZE AFTER B PUSH!" << endl;
		return false;
	}

	queueB.Pop();

	if (!checkQueueSize(queueA, queueB)) {
		cout << TAG << "QUEUE A AND QUEUE B DIFFERS IN SIZE!" << endl;
		return false;
	}

	queueA.Push(99);
	queueA.Pop();

	return true;
}

/*
	Test queue usage w/o pointer
*/
bool testBasicIntQueueNoPointer() {
	cout << TAG << "|---------BEGIN testNonPointerQueue" << endl;

	Queue<int> testQueue;

	initSampleIntQueue(testQueue, 5);

	int size = testQueue.GetSize();

	cout << TAG << "testQueue Size: " << size << endl;

	testQueue.Print();

	size -= 3;

	testQueue.Pop();
	testQueue.Pop();
	testQueue.Pop();

	testQueue.Print();

	if (size != testQueue.GetSize()) {
		cout << TAG << "size is wrong, expect : " << size << "got : " << testQueue.GetSize() << endl;
		return false;
	}

	while (!testQueue.IsEmpty()) {
		testQueue.Pop();
	}

	testQueue.Print();

	cout << TAG << "testQueue Size: " << testQueue.GetSize() << endl;

	if (testQueue.GetSize() != 0) {
		cout << TAG << "size is wrong, expect 0" << endl;
		return false;
	}

	cout << TAG << "|---------END testPushPrintPopBracket SUCCESS" << endl << endl;
	return true;
}

/*
	Test queue usage w. pointer
*/
bool testBasicIntQueueWithPointer() {
	cout << TAG << "|---------BEGIN testBasicIntQueueWithPointer" << endl;

	Queue<int> * testQueue = new Queue <int>();

	initSampleIntQueue(*testQueue, 3);

	int size = testQueue->GetSize();

	cout << TAG << "testQueue Size: " << size << endl;

	size += 4;

	testQueue->Push(0);
	testQueue->Push(2);
	testQueue->Push(3);
	testQueue->Push(5);

	testQueue->Print();

	cout << TAG << "testQueue Size: " << testQueue->GetSize() << endl;

	if (size != testQueue->GetSize()) {
		cout << TAG << "size is wrong, expect : " << size << "got : " << testQueue->GetSize() << endl;
		return false;
	}

	cout << TAG << testQueue->Pop() << endl;
	cout << TAG << testQueue->Pop() << endl;
	cout << TAG << testQueue->Pop() << endl;

	testQueue->Print();

	delete testQueue;

	cout << TAG << "|---------END testBasicIntQueueWithPointer SUCCESS" << endl << endl;
	return true;
}

/*
	Test copy constructor
*/
bool testCopyConstructor() {
	cout << TAG << "|---------BEGIN testCopyConstructor" << endl;

	Queue<int> * queueA = new Queue <int>();

	initSampleIntQueue(*queueA, 5);

	Queue<int> * queueB = new Queue <int>(*queueA);

	if (!compareTwoQueue<int>(*queueA, *queueB)) {
		cout << TAG << "QUEUE A and QUEUE B are not similar" << endl;
		return false;
	}

	delete queueA;
	delete queueB;

	cout << TAG << "|---------END testCopyConstructor SUCCESS" << endl << endl;
	return true;
}

/*
	Test copy operator
*/
bool testCopyOperator() {
	cout << TAG << "|---------BEGIN testCopyOperator" << endl;

	Queue<int> queueA;

	initSampleIntQueue(queueA, 4);

	Queue<int> queueB = queueA;

	if (!compareTwoQueue(queueA, queueB)) {
		cout << TAG << "QUEUE A and QUEUE B are not similar" << endl;
		return false;
	}

	cout << TAG << "|---------END testCopyOperator SUCCESS" << endl << endl;
	return true;
}


/*
	Test item queue
*/
bool testItemQueue() {
	cout << TAG << "|---------BEGIN testItemQueue" << endl;

	Queue<Item> queueA;

	initSampleItemQueue(queueA, 5);

	Queue<Item> queueB = Queue<Item>(queueA);

	Queue<Item> queueC = queueA;

	Queue<Item> queueD = queueB;


	if (!compareTwoQueue(queueA, queueB)) {
		cout << TAG << "QUEUE A and QUEUE B are not similar" << endl;
		return false;
	}

	if (!compareTwoQueue(queueC, queueD)) {
		cout << TAG << "QUEUE C and QUEUE D are not similar" << endl;
		return false;
	}

	if (!compareTwoQueue(queueB, queueC)) {
		cout << TAG << "QUEUE B and QUEUE C are not similar" << endl;
		return false;
	}

	if (!compareTwoQueue(queueA, queueD)) {
		cout << TAG << "QUEUE A and QUEUE D are not similar" << endl;
		return false;
	}
	
	cout << TAG << "|---------END testItemQueue SUCCESS" << endl << endl;
	return true;
}

int main() {

	int exitCode = 0;

	if (!testBasicIntQueueNoPointer()) {
		cout << TAG << "Failed: testBasicIntQueueNoPointer" << endl;
		exitCode = 1;
	}

	if (!testBasicIntQueueWithPointer()) {
		cout << TAG << "Failed: testBasicIntQueueWithPointer" << endl;
		exitCode = 1;
	}

	if (!testCopyConstructor()) {
		cout << TAG << "Failed: testCopyConstructor" << endl;
		exitCode = 1;
	}

	if (!testCopyOperator()) {
		cout << TAG << "Failed: testCopyOperator" << endl;
		exitCode = 1;
	}

	if (!testItemQueue()) {
		cout << TAG << "Failed: testItemQueue" << endl;
		exitCode = 1;
	}

	_CrtDumpMemoryLeaks();

	cout << TAG << "Press enter to finish . . .";
	getchar();
	return exitCode;
}