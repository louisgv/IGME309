#pragma once
/*
	Header for a tepmplated linked list node
	Author: LAB
	License: MIT

*/

#include <iostream>

using namespace std;

template <typename T>
class LinkedListNode
{
private:
	const char* TAG = "[NODE]\t\t";
	
	T data;

	LinkedListNode<T>* nextPtr;
	
	LinkedListNode<T>* prevPtr;

public:
	LinkedListNode(const T& nodeData);
	~LinkedListNode();

	/* Get the ptr to next node in the linked list*/
	LinkedListNode<T>* GetNextPtr();

	/* Get the ptr to previous node in the linked list */
	LinkedListNode<T>* GetPreviousPtr();

	/* Set the ptr to previous node */
	void SetPreviousPtr (LinkedListNode<T> * node);

	/* Set the ptr to next node */
	void SetNextPtr (LinkedListNode<T> * node);

	/* Set next ptr to null */
	void RemoveNextPtr();

	/* Set prev ptr to null */
	void RemovePrevPtr();

	/* Get the ptr to the data */
	T GetData();

	/* Check if this is the end node */
	bool IsEnd();
};

template<typename T>
inline LinkedListNode<T>::LinkedListNode(const T& nodeData)
{
	// cout << TAG << "Constructor called . . . " << endl;

	data = nodeData;
	nextPtr = nullptr;
	prevPtr = nullptr;
}

template<typename T>
inline LinkedListNode<T>::~LinkedListNode()
{
	// cout << TAG << "Destructor called . . . " << endl;
	data = NULL;
	prevPtr = nullptr;
	if (!IsEnd()) {
		delete nextPtr;
	}
}

template<typename T>
inline LinkedListNode<T> * LinkedListNode<T>::GetNextPtr()
{
	return nextPtr;
}

template<typename T>
inline LinkedListNode<T>* LinkedListNode<T>::GetPreviousPtr()
{
	return prevPtr;
}

template<typename T>
inline void LinkedListNode<T>::SetPreviousPtr(LinkedListNode<T>* node)
{
	prevPtr = node;
}

template<typename T>
inline void LinkedListNode<T>::SetNextPtr(LinkedListNode<T>* node)
{
	nextPtr = node;
}

template<typename T>
inline void LinkedListNode<T>::RemoveNextPtr()
{
	nextPtr = nullptr;
}

template<typename T>
inline void LinkedListNode<T>::RemovePrevPtr()
{
	prevPtr = nullptr;
}

template<typename T>
inline T LinkedListNode<T>::GetData()
{
	return data;
}

template<typename T>
inline bool LinkedListNode<T>::IsEnd()
{
	return nextPtr == nullptr;
}
