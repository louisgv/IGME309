/*
	Queue Header file
	Author: lab@mail.rit.edu
	IGME309.01

	LICENSE: MIT
*/

#pragma once

#include "list.h"

#include <iostream>

using namespace std;

template<typename T>
class Queue : public DoubleLinkedList <T>
{
private:
	const char* TAG = "[QUEUE]\t\t";
	
public:
	/* Print with queue flow */
	void Print();

	/* Push an item into the queue */
	void Push(T item);

	/* Pop the last item from the queue */
	T Pop();
};

template<typename T>
inline void Queue<T>::Print()
{
	DoubleLinkedList<T>::Print("<-");
}

template<typename T>
inline void Queue<T>::Push(T item)
{
	cout << TAG << "Pushing in " << item << endl;

	// If list is empty, add to tail
	if (DoubleLinkedList<T>::IsEmpty()) {
		DoubleLinkedList<T>::AddTail(item);
		return;
	}

	LinkedListNode<T> * traversePtr = DoubleLinkedList<T>::GetHeadPtr();

	LinkedListNode<T> * tailPtr = DoubleLinkedList<T>::GetTailPtr();

	try
	{
		// Quick add to tail
		if (tailPtr == nullptr || item >= tailPtr->GetData()) {
			DoubleLinkedList<T>::AddTail(item);
			return;
		}

		// Quick add to head
		if (item <= traversePtr->GetData()) {
			DoubleLinkedList<T>::AddHead(item);
			return;
		}

		// Traverse through linked list until desired position reached,
		// then insert itself before the traversePtr and after the 
		// ptr previous to traversePtr
		while (item > traversePtr->GetData() && traversePtr->GetNextPtr() != nullptr) {
			traversePtr = traversePtr->GetNextPtr();
		}

		LinkedListNode<T> * prevTraversePtr = traversePtr->GetPreviousPtr();

		LinkedListNode<T> * newNode = new LinkedListNode<T>(item);

		if (prevTraversePtr != nullptr) {
			prevTraversePtr->SetNextPtr(newNode);
			newNode->SetPreviousPtr(prevTraversePtr);
		}

		newNode->SetNextPtr(traversePtr);

		traversePtr->SetPreviousPtr(newNode);

		DoubleLinkedList<T>::size++;
	}
	catch (const exception& e)
	{
		// Catching any exception related to T not implementing 
		// comparator ops
		cerr << TAG << e.what() << endl;
	}
}

template<typename T>
inline T Queue<T>::Pop()
{
	LinkedListNode<T> * trimmedPtr = DoubleLinkedList<T>::TrimHead();

	if (trimmedPtr == nullptr) {
		return NULL;
	}

	// Get data and clean up the pointer
	T data = trimmedPtr->GetData();

	delete trimmedPtr;

	cout << TAG << "Popping out " << data << " . . . " << endl;

	return data;
}
