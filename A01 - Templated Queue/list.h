#pragma once
/*
	Header for a tepmplated double linked list
	Author: LAB
	License: MIT

	NOTE:
		 const T	: a copy of the object that cannot be modified
		 T&			: a non-const reference to the object
		 const T&	: a const reference to the object
*/

#include "node.h"

#include <iostream>

using namespace std;

template<typename T>
class DoubleLinkedList {
private:
	const char* TAG = "[LIST]\t\t";

	LinkedListNode<T> * headPtr;
	LinkedListNode<T> * tailPtr;

protected:
	int size;

public:
	DoubleLinkedList();

	/* Copy constructor used to create a new list from another */
	DoubleLinkedList(const DoubleLinkedList<T>& other);

	/* Assignment operator used to copy to another list */
	DoubleLinkedList<T> & operator= (const DoubleLinkedList<T> & other);

	~DoubleLinkedList();

	/* Print out content of the list */
	void Print(const char* separator);

	/* Check if list is empty */
	bool IsEmpty() const;

	/* Get size of the list */
	int GetSize();

	/* Get ptr to node at index*/
	LinkedListNode<T>* GetNodePtr(int index);

	/* Get ptr to the head */
	LinkedListNode<T>* GetHeadPtr();

	/* Get ptr to the tail */
	LinkedListNode<T>* GetTailPtr();

	/* Add a new tail node by reference to a data piece */
	void AddTail(const T& data);

	/* Add a new head node by reference to a data piece */
	void AddHead(const T& data);

	/* Remove node at index and return ptr to it */
	LinkedListNode<T>* RemoveNode(int index);

	/* Trim the tail of the linked list and return a ptr to the removed node*/
	LinkedListNode<T>* TrimTail();

	/* Trim the head of the linked list and return a ptr to the removed node */
	LinkedListNode<T>* TrimHead();
};

template<typename T>
inline DoubleLinkedList<T>::DoubleLinkedList()
{
	cout << TAG << "Constructor called . . . " << endl;

	headPtr = nullptr;
	tailPtr = nullptr;
	size = 0;
}

template<typename T>
inline DoubleLinkedList<T>::DoubleLinkedList(const DoubleLinkedList<T>& other)
{
	cout << TAG << "Copy Constructor called . . . " << endl;

	// Clean up current list
	delete headPtr;
	size = 0;

	LinkedListNode<T> * otherPtr = other.headPtr;

	while (otherPtr != nullptr) {
		AddTail(otherPtr->GetData());
		otherPtr = otherPtr->GetNextPtr();
	}
}

template<typename T>
inline DoubleLinkedList<T>& DoubleLinkedList<T>::operator=(const DoubleLinkedList<T>& other)
{
	cout << TAG << "Copy Operator called . . . " << endl;

	if (this == &other)
	{
		return *this;
	}
	DoubleLinkedList<T> output = new DoubleLinkedList<T>(other);

	return output;
}

template<typename T>
inline DoubleLinkedList<T>::~DoubleLinkedList()
{
	cout << TAG << "Destructor called . . . " << endl;

	delete headPtr;
	tailPtr = nullptr;
	size = 0;
}

template<typename T>
inline void DoubleLinkedList<T>::Print(const char* separator)
{
	if (IsEmpty())
	{
		cout << TAG << "[]" << endl;
		return;
	}
	cout << TAG << "[ |";
	
	LinkedListNode<T> * nodePtr = headPtr;
	
	while (nodePtr != nullptr)
	{
		cout << " " << separator << " " << nodePtr->GetData();

		nodePtr = nodePtr->GetNextPtr();
	}  

	cout << " ]" << endl;

	nodePtr = nullptr;
}

template<typename T>
inline bool DoubleLinkedList<T>::IsEmpty() const
{
	return size == 0;
}

template<typename T>
inline int DoubleLinkedList<T>::GetSize()
{
	return size;
}

template<typename T>
inline LinkedListNode<T>* DoubleLinkedList<T>::GetNodePtr(int index)
{
	LinkedListNode<T> * ptr = headPtr;
	while (index-- > 0) {
		ptr = ptr->GetNextPtr();
	}
	return ptr;
}

template<typename T>
inline LinkedListNode<T>* DoubleLinkedList<T>::GetHeadPtr()
{
	return headPtr;
}

template<typename T>
inline LinkedListNode<T>* DoubleLinkedList<T>::GetTailPtr()
{
	return tailPtr;
}

template<typename T>
inline void DoubleLinkedList<T>::AddTail(const T & data)
{
	LinkedListNode<T>* newNodePtr = new LinkedListNode<T>(data);

	if (size == 0) {
		headPtr = newNodePtr;
	}
	else if (size == 1) {
		tailPtr = newNodePtr;
		headPtr->SetNextPtr(tailPtr);
		tailPtr->SetPreviousPtr(headPtr);
	}
	else {
		tailPtr->SetNextPtr(newNodePtr);
		newNodePtr->SetPreviousPtr(tailPtr);
		tailPtr = newNodePtr;
	}

	size += 1;
}

template<typename T>
inline void DoubleLinkedList<T>::AddHead(const T & data)
{
	LinkedListNode<T>* newNodePtr = new LinkedListNode<T>(data);

	if (size == 0)
	{
		headPtr = newNodePtr;
	}
	else if (size == 1) {
		tailPtr = headPtr;
		headPtr = newNodePtr;
		headPtr->SetNextPtr(tailPtr);
		tailPtr->SetPreviousPtr(headPtr);
	}
	else {
		newNodePtr->SetNextPtr(headPtr);
		headPtr->SetPreviousPtr(newNodePtr);
		headPtr = newNodePtr;
	}

	size += 1;
}

template<typename T>
inline LinkedListNode<T>* DoubleLinkedList<T>::RemoveNode(int index)
{
	if (IsEmpty() || index < 0 || index >= size) {
		return nullptr;
	}

	size -= 1;

	LinkedListNode<T> * nodePtr = headPtr;

	while (index-- > 0) {
		nodePtr = headPtr->GetNextPtr();
	}

	LinkedListNode<T> * nextPtr = nodePtr->GetNextPtr();
	LinkedListNode<T> * prevPtr = nodePtr->GetPreviousPtr();

	nextPtr->SetPreviousPtr(prevPtr);
	prevPtr->SetNextPtr(nextPtr);

	return nodePtr;
}

template<typename T>
inline LinkedListNode<T>* DoubleLinkedList<T>::TrimTail()
{
	if (IsEmpty()) {
		return nullptr;
	}

	size -= 1;

	LinkedListNode<T>* tempPtr;

	if (IsEmpty()) {
		tempPtr = headPtr;
		headPtr = nullptr;
		tailPtr = nullptr;
	}
	else {
		tempPtr = tailPtr;

		tailPtr = tailPtr->GetPreviousPtr();

		tailPtr->RemoveNextPtr();
	}
	tempPtr->RemoveNextPtr();
	tempPtr->RemovePrevPtr();

	return tempPtr;
}

template<typename T>
inline LinkedListNode<T>* DoubleLinkedList<T>::TrimHead()
{
	if (IsEmpty()) {
		return nullptr;
	}

	size -= 1;

	LinkedListNode<T>* tempPtr = headPtr;

	if (IsEmpty()) {
		headPtr = nullptr;
		tailPtr = nullptr;
	}
	else {
		headPtr = headPtr->GetNextPtr();
		headPtr->RemovePrevPtr();
	}

	tempPtr->RemoveNextPtr();
	tempPtr->RemovePrevPtr();

	return tempPtr;
}
