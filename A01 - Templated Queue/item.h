#pragma once
/*
	Item class used for testing with the Stack
	AUTHOR: LAB
	LICENSE: MIT
*/
#include <iostream>

using namespace std;

class Item
{
private:
	int id;
	friend std::ostream & operator<<(std::ostream &os, const Item& p);

public:
	Item();
	Item(int id);
	~Item();

	// Return id of the item
	int GetId() const;
};

inline bool operator==(const Item& lhs, const Item& rhs) {
	return lhs.GetId() == rhs.GetId();
}
inline bool operator< (const Item& lhs, const Item& rhs) { 
	return lhs.GetId() < rhs.GetId();
}
inline bool operator!=(const Item& lhs, const Item& rhs) { return !operator==(lhs, rhs); }
inline bool operator> (const Item& lhs, const Item& rhs) { return  operator< (rhs, lhs); }
inline bool operator<=(const Item& lhs, const Item& rhs) { return !operator> (lhs, rhs); }
inline bool operator>=(const Item& lhs, const Item& rhs) { return !operator< (lhs, rhs); }

