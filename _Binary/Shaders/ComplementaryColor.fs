#version 330

in vec3 Color;

uniform vec3 SolidColor = vec3(-1,-1,-1);

uniform float ComplimentaryFlag = 0;

uniform vec3 WhiteColor = vec3(1, 1, 1);

vec3 FinalColor = vec3(0, 0, 0);

out vec4 Fragment;

void main()
{
	FinalColor = Color;
	
	if(SolidColor.r != -1.0 && SolidColor.g != -1.0 && SolidColor.b != -1.0)
		FinalColor = SolidColor;
	
	if(ComplimentaryFlag != 0) {
		FinalColor = WhiteColor - FinalColor;
	}

	Fragment = vec4(FinalColor, 1);

	return;
}